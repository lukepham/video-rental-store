package com.rental.store.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class RentOrder {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Customer customer;

    private Integer numOfDays;

    public RentOrder(Customer customer, Integer numOfDays) {
        this.customer = customer;
        this.numOfDays = numOfDays;
    }
}
