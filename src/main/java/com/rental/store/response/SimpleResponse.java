package com.rental.store.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SimpleResponse {

    public static final SimpleResponse OK = new SimpleResponse("Ok", null, null);

    private String status;
    private String errorMsg;
    private Object data;

}
