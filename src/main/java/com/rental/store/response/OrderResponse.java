package com.rental.store.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class OrderResponse {

    private String status;
    private Integer price;
    private Long orderId;

}
