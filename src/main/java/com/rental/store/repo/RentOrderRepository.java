package com.rental.store.repo;

import com.rental.store.model.RentOrder;
import org.springframework.data.repository.CrudRepository;

public interface RentOrderRepository extends CrudRepository<RentOrder, Long> {
}