package com.rental.store.service;

import com.rental.store.model.Film;
import com.rental.store.model.FilmType;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class BusinessService {

    public static final int PREMIUM_PRICE = 40;
    public static final int BASIC_PRICE = 30;

    private static final Integer REGULAR_DAYS_THRESHOLD = 3;
    private static final Integer OLD_DAYS_THRESHOLD = 5;

    private HashMap<FilmType, Function<Integer, Integer>> priceCalculator;
    private HashMap<FilmType, Integer> surchargePriceMap;

    private final HashMap<FilmType, Integer> bonusMap;

    public BusinessService() {
        priceCalculator = new HashMap<>();
        priceCalculator.put(FilmType.NEW, this::getNewPrice);
        priceCalculator.put(FilmType.REGULAR, this::getRegularPrice);
        priceCalculator.put(FilmType.OLD, this::getOldPrice);

        surchargePriceMap = new HashMap<>();
        surchargePriceMap.put(FilmType.NEW, PREMIUM_PRICE);
        surchargePriceMap.put(FilmType.REGULAR, BASIC_PRICE);
        surchargePriceMap.put(FilmType.OLD, BASIC_PRICE);

        bonusMap = new HashMap<>();
        bonusMap.put(FilmType.NEW, 2);
        bonusMap.put(FilmType.REGULAR, 1);
        bonusMap.put(FilmType.OLD, 1);
    }

    private Integer getNewPrice(Integer numOfDays) {
        return PREMIUM_PRICE * numOfDays;
    }

    private Integer getRegularPrice(Integer numOfDays) {
        return getThreshholdPrice(REGULAR_DAYS_THRESHOLD, numOfDays);
    }

    private Integer getOldPrice(Integer numOfDays) {
        return getThreshholdPrice(OLD_DAYS_THRESHOLD, numOfDays);
    }

    private Integer getThreshholdPrice(Integer threshold, Integer numOfDays) {
        if (numOfDays <= threshold) {
            return BASIC_PRICE;
        }
        return BASIC_PRICE * (numOfDays - threshold);
    }

    public Integer calculatePrice(Integer numOfDays, List<Film> films) {
        Integer totalPrice = 0;
        for (Film film : films) {
            FilmType type = film.getType();
            Function<Integer, Integer> calcFunc = priceCalculator.get(type);
            Integer filmPrice = calcFunc.apply(numOfDays);
            totalPrice += filmPrice;
        }
        return totalPrice;
    }

    public Integer calculateBonus(List<Film> films) {
        Integer bonus = 0;
        for (Film film : films) {
            FilmType type = film.getType();
            Integer filmBonus = bonusMap.get(type);
            bonus += filmBonus;
        }
        return bonus;
    }

    public Integer calculateSurcharge(Integer daysOverdue, List<Film> films) {
        Integer totalPrice = 0;
        for (Film film : films) {
            FilmType type = film.getType();
            Integer filmPrice = surchargePriceMap.get(type);
            totalPrice += filmPrice * daysOverdue;
        }
        return totalPrice;
    }
}
