package com.rental.store.service;

import com.rental.store.model.Customer;
import com.rental.store.model.Film;
import com.rental.store.model.OrderFilm;
import com.rental.store.model.RentOrder;
import com.rental.store.repo.CustomerRepository;
import com.rental.store.repo.FilmRepository;
import com.rental.store.repo.OrderFilmRepository;
import com.rental.store.repo.RentOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StorageService {

    @Autowired
    private LockService lockService;

    @Autowired
    private CustomerRepository customerRepo;
    @Autowired
    private FilmRepository filmRepo;
    @Autowired
    private RentOrderRepository rentOrderRepo;
    @Autowired
    private OrderFilmRepository orderFilmRepo;


    public void saveCustomer(Customer customer) {
        customerRepo.save(customer);
    }

    public Customer findCustomer(Long customerId) {
        return customerRepo.findOne(customerId);
    }

    public List<Customer> findAllCustomers() {
        Iterable<Customer> customers = customerRepo.findAll();
        return toList(customers);
    }

    public void saveFilm(Film film) {
        filmRepo.save(film);
    }

    public List<Film> findAllFilms() {
        Iterable<Film> films = filmRepo.findAll();
        return toList(films);
    }

    public List<Film> findFilms(List<Long> filmIds) {
        Iterable<Film> films = filmRepo.findAll(filmIds);
        return toList(films);
    }

    @Transactional
    public void saveRentOrder(RentOrder order, List<Film> films) {
        rentOrderRepo.save(order);
        List<OrderFilm> orderFilms = films.stream()
                .map(film -> new OrderFilm(order, film)).collect(Collectors.toList());
        orderFilmRepo.save(orderFilms);
    }

    public List<RentOrder> findAllOrders() {
        Iterable<RentOrder> orders = rentOrderRepo.findAll();
        return toList(orders);
    }

    public <T> List<T> toList(Iterable<T> iterable) {
        List<T> list = new ArrayList<>();
        iterable.forEach(list::add);
        return list;
    }

    @Transactional
    public void removeRentOrder(Long rentOrderId, List<OrderFilm> orderFilms) {
        rentOrderRepo.delete(rentOrderId);
        orderFilmRepo.delete(orderFilms);
    }

    public List<OrderFilm> findOrderFilms(Long rentOrderId) {
        return orderFilmRepo.findByOrderId(rentOrderId);
    }

    public void updateBonus(Long customerId, Integer bonus) {
        Object customerLock = lockService.getCustomerLock(customerId);
        synchronized (customerLock) {
            Customer customer = customerRepo.findOne(customerId);
            customer.addBonus(bonus);
            customerRepo.save(customer);
        }
    }
}
