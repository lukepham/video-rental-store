package com.rental.store.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;

import static spark.Spark.port;
import static spark.Spark.post;

public class RoutesService {

    @Autowired
    private RestService restService;

    @Autowired
    private Gson gson;

    @Value("${server-port}")
    private Integer serverPort;

    @PostConstruct
    public void init() {
        port(serverPort);
        setupRoutes();
    }

    private void setupRoutes() {
        post("/add_customer", restService::addCustomer, gson::toJson);
        post("/get_customer", restService::getCustomer, gson::toJson);
        post("/get_all_customers", restService::getAllCustomers, gson::toJson);

        post("/add_film", restService::addFilm, gson::toJson);
        post("/get_all_films", restService::getAllFilms, gson::toJson);

        post("/create_order", restService::createOrder, gson::toJson);
        post("/get_all_orders", restService::getAllOrders, gson::toJson);
        post("/complete_order", restService::completeOrder, gson::toJson);
    }
}
