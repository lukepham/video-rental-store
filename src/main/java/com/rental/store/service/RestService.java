package com.rental.store.service;

import com.rental.store.model.*;
import com.rental.store.response.OrderResponse;
import com.rental.store.response.SimpleResponse;
import com.rental.store.response.SurchargeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import spark.Request;
import spark.Response;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RestService {

    @Autowired
    private StorageService storage;
    @Autowired
    private BusinessService business;

    public Object addCustomer(Request req, Response res) {
        String firstName = req.queryParams("firstName");
        String lastName = req.queryParams("lastName");
        Customer customer = new Customer(firstName, lastName);
        storage.saveCustomer(customer);
        return SimpleResponse.OK;
    }

    public Object getCustomer(Request req, Response res) {
        Long customerId = getLong(req, "id");
        Customer customer = storage.findCustomer(customerId);
        return new SimpleResponse("Ok", null, customer);
    }

    public Object getAllCustomers(Request req, Response res) {
        List<Customer> customers = storage.findAllCustomers();
        return new SimpleResponse("Ok", null, customers);
    }

    public Object addFilm(Request req, Response res) {
        String title = req.queryParams("title");
        String typeStr = req.queryParams("type");
        FilmType type = FilmType.valueOf(typeStr);
        Film film = new Film(title, type);
        storage.saveFilm(film);
        return SimpleResponse.OK;
    }

    public Object getAllFilms(Request req, Response res) {
        List<Film> films = storage.findAllFilms();
        return new SimpleResponse("Ok", null, films);
    }

    public Object createOrder(Request req, Response res) {
        Long customerId = getLong(req, "customerId");
        List<Long> filmIds = getLongs(req, "filmId");
        Integer numOfDays = getInt(req, "numOfDays");
        Customer customer = storage.findCustomer(customerId);
        List<Film> films = storage.findFilms(filmIds);
        RentOrder order = new RentOrder(customer, numOfDays);
        storage.saveRentOrder(order, films);
        Integer price = business.calculatePrice(numOfDays, films);
        Integer bonus = business.calculateBonus(films);
        storage.updateBonus(customerId, bonus);
        return new OrderResponse("Ok", price, order.getId());
    }

    public Object getAllOrders(Request req, Response res) {
        List<RentOrder> orders = storage.findAllOrders();
        return new SimpleResponse("Ok", null, orders);
    }

    public Object completeOrder(Request req, Response res) {
        Long rentOrderId = getLong(req, "orderId");
        Integer daysOverdue = getInt(req, "daysOverdue");
        List<OrderFilm> orderFilms = storage.findOrderFilms(rentOrderId);
        List<Film> films = orderFilms.stream().map(OrderFilm::getFilm).collect(Collectors.toList());
        Integer surcharge = business.calculateSurcharge(daysOverdue, films);
        storage.removeRentOrder(rentOrderId, orderFilms);
        return new SurchargeResponse("Ok", rentOrderId, surcharge);
    }

    private Long getLong(Request req, String paramName) {
        String stringValue = req.queryParams(paramName);
        return Long.parseLong(stringValue);
    }

    private List<Long> getLongs(Request req, String paramName) {
        String[] stringValues = req.queryParamsValues(paramName);
        return Arrays.stream(stringValues).map(Long::parseLong).collect(Collectors.toList());
    }

    private Integer getInt(Request req, String paramName) {
        String stringValue = req.queryParams(paramName);
        return Integer.parseInt(stringValue);
    }
}
