package com.rental.store;

import com.rental.store.service.RoutesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class StoreApp implements CommandLineRunner {

    @Autowired
    private ApplicationContext context;

    public static void main(String[] args) throws Exception {

        SpringApplication.run(StoreApp.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        context.getBean(RoutesService.class);
    }
}