package com.rental.store.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rental.store.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MainConfig {

    @Bean
    public RoutesService routesService() {
        return new RoutesService();
    }

    @Bean
    public RestService restService() {
        return new RestService();
    }

    @Bean
    public BusinessService businessService() {
        return new BusinessService();
    }

    @Bean
    public StorageService storageService() {
        return new StorageService();
    }

    @Bean
    public LockService lockService() {
        return new LockService();
    }

    @Bean
    public Gson gson() {
        return new GsonBuilder().setPrettyPrinting().create();
    }
}
